FROM alpine:3.12.0
RUN apk update && apk add --no-cache nodejs npm && mkdir /.npm && chmod -R g=u /.npm

USER 1001

CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way. Please visit www.lackastack.net to see how to use it!"]
